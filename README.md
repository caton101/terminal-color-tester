# Terminal Color Tester

This program iterates over all the ASCII color codes and writes them to the terminal.

## Usage

`python3 terminalColorTester.py`

## Screenshots

### MacOS:

![screenshot](img/screenshot-macos.png)

### Linux:

![screenshot](img/screenshot-linux.png)
